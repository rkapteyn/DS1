#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "stack.h"

struct stack {
    int index, aantalPush, aantalPop, maxStackSize;
    int stack_array[STACK_SIZE];
};

//dit heel erg aanpassen want je weet de regels
int presedence(char c) {
	if(c == '^')/* exponent operator, highest precedence*/
	{
		return(3);
	}
	else if(c == '*' || c == '/')
	{
		return(2);
	}
	else if(c == '+' || c == '-')/* lowest precedence */
	{
		return(1);
	}
	else
	{
		return(0);
	}
}

int main(int argc, char *argv[]) {
    char c;
    int nummer;

    if (argc != 2) {
        printf("usage: %s \"infix_expr\"\n", argv[0]);
        return 1;
    }
    char *input = argv[1];
    struct stack *s = stack_init();
    nummer = 0;

    while ((c = *input++) != 0) {
        // if (c == ' ') {
        //     nummer = 0;
        // }
        // check for operand. alle letters en cijfers.
        if (isdigit(c) || isalpha(c)) {
            if (nummer == 0) 
                printf(" ");
            printf("%c", c);
            nummer = 1;
        }

            // check for operator
        if (c == '+' || c == '-' || c == '*' || c == '/' || c == '^' || c == '~') {
            if (stack_empty(s))
                stack_push(s, c);
            else {
                /*Als de presedence groter is dan wat er in de stack aanwezig is.*/
                if (presedence(c) > presedence((char) stack_peek(s)))
                    stack_push(s, c);
                else {
                    while(presedence(c) <= presedence((char) stack_peek(s)) && !stack_empty(s)) {
                        if (nummer == 1) {
                            printf(" ");
                            nummer = 0;
                        }
                        printf("%c ", stack_pop(s));
                    }
                    stack_push(s, c);
                }
            }
            nummer = 0;
        }
        if (c == '(')
            stack_push(s, c);
        if (c == ')') {
            while ((c = (char) stack_pop(s)) != '(' && !stack_empty(s)){
                if (nummer == 1) {
                    printf(" ");
                    nummer = 0;
                }
                printf("%c ", c);
            }
        }
 
    }
    // dump stack
    if (nummer == 1)
        printf(" ");
    while (!stack_empty(s)) printf("%c ", stack_pop(s));
    fprintf(stderr, "stats %d %d %d", s->aantalPush, s->aantalPop, s->maxStackSize);
    stack_cleanup(s);
    return 0;
}
