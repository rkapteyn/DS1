#include <stdlib.h>
#include "stack.h"

struct stack {
    int index, aantalPush, aantalPop, maxStackSize;     // index points to the first free spot in the stack
    int stack_array[STACK_SIZE];                        // in stack_array the values pushed on the stack are stored
};

struct stack *stack_init() {
    struct stack *s = malloc(sizeof(struct stack));
    s->index = 0;
    s->aantalPush = 0;
    s->aantalPop = 0;
    s->maxStackSize = 0;
    return s;
}

void stack_cleanup(struct stack *s) {
    free(s);
}

int stack_push(struct stack *s, int c) {
    if (s->index == STACK_SIZE) {
        return 1;
    }
    (s->aantalPush)++;
    s->stack_array[s->index++] = c;
    if (s->index > s->maxStackSize) {
        s->maxStackSize = s->index;
    }
    return 0;
}

int stack_pop(struct stack *s) {
    if(stack_empty(s)) {
        return -1;
    }
    (s->aantalPop)++;
    return s->stack_array[--(s->index)];
}

int stack_peek(struct stack *s) {
    if(stack_empty(s)){
        return -1;
    }
    return s->stack_array[s->index - 1];
}

int stack_empty(struct stack *s) {
    if (s->index == 0)
        return(1);
    return 0;
}